### Read me

This is a little web app to display climate data.

![screenshot](the_app/screenshot.png)

#### Installation

You need python 3 (tested with 3.7) and the following packages to run the app:

* cartopy
* Fiona
* flask
* flask-wtf
* matplotlib
* netcdf4
* numpy
* pandas
* scipy
* shapely
* wtform
* xarray
* pycountry (from conda-forge)

If you don't know how to create a python environment and install packages: https://docs.conda.io/projects/conda/en/latest/user-guide/tasks/manage-environments.html

#### Test

Before testing the app, you need to prepare the data for it:

```
python data_preparation.py
```

To test the app, you need to run *run.py* from within *the_app*

```
cd the_app
python the_app/run.py
```

#### Design

* *some_location_where_large_raw_data_is_stored*: Test data and shape-files are stored in *some_location_where_large_raw_data_is_stored*. This path is specified in the *data_preparation.py* script. You can load other data from your computer if you want to.

* *climate_data_on_shapes* a python class which allows you to manage climate data. It is initialised with some shape-file and has functions to create masks for the shape-file, compute area averages etc. see https://gitlab.com/PeterPeter/climate_data_on_shapes

* *data_preparation.py* uses *climate_data_on_shapes* to prepare some small data portions that can be used by the online tool (stored in *small_data*).

* *the_app* is a flask app that requires *climate_data_on_shapes* and the data stored in *small_data*.
